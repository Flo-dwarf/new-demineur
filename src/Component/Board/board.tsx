import { Cell, CellState, CellValue } from "../Enum/enum";
import { COLS, NUMBER_MINE, ROWS } from "../Enum/values";

export const generateBoard = () => {
  let caseBox: Cell[][] = [];
  // Génère la grille de cases
  for (let row = 0; row < ROWS; row++) {
    caseBox.push([]);
    for (let col = 0; col < COLS; col++) {
      caseBox[row].push({
        value: CellValue.nothing,
        state: CellState.hide,
      });
    }
  }

  // Place des mines aléatoirement
  let bombInBoard = 0;
  do {
    const randomRow = Math.floor(Math.random() * Math.floor(ROWS));
    const randomCol = Math.floor(Math.random() * Math.floor(COLS));
    const randomBomb = caseBox[randomRow][randomCol];

    if (randomBomb.value !== CellValue.bomb) {
      caseBox = caseBox.map((row, indexRow) =>
        row.map((box, indexCol) => {
          if (randomRow === indexRow && randomCol === indexCol) {
            return { ...box, value: CellValue.bomb };
          }
          return box;
        })
      );
      bombInBoard++;
    }
  } while (bombInBoard < NUMBER_MINE);

  // Place les chiffres ou rien autour des bombes
  for (let row = 0; row < ROWS; row++) {
    for (let col = 0; col < COLS; col++) {
      
      const aroundBox = caseBox[row][col];

      if (caseBox[row][col].value === CellValue.bomb) {
        continue;
      }

      let numberAroundBomb = 0;
      const boxUpleft = row > 0 && col > 0 ? caseBox[row - 1][col - 1] : null;
      const boxUpMiddle = row > 0 ? caseBox[row - 1][col] : null;
      const boxUpRight =
        row > 0 && col < COLS - 1 ? caseBox[row - 1][col + 1] : null;
      const boxMiddleLeft = col > 0 ? caseBox[row][col - 1] : null;
      const boxMiddleRight = col < COLS - 1 ? caseBox[row][col + 1] : null;
      const boxBottomleft =
        row < ROWS - 1 && col > 0 ? caseBox[row + 1][col - 1] : null;
      const boxBottomMiddle = row < ROWS - 1 ? caseBox[row + 1][col] : null;
      const boxBottomRight =
        row < ROWS - 1 && col < COLS - 1 ? caseBox[row + 1][col + 1] : null;

      if (boxUpleft?.value === CellValue.bomb) {
        numberAroundBomb++;
      }
      if (boxUpMiddle?.value === CellValue.bomb) {
        numberAroundBomb++;
      }
      if (boxUpRight?.value === CellValue.bomb) {
        numberAroundBomb++;
      }
      if (boxMiddleLeft?.value === CellValue.bomb) {
        numberAroundBomb++;
      }
      if (boxMiddleRight?.value === CellValue.bomb) {
        numberAroundBomb++;
      }
      if (boxBottomleft?.value === CellValue.bomb) {
        numberAroundBomb++;
      }
      if (boxBottomMiddle?.value === CellValue.bomb) {
        numberAroundBomb++;
      }
      if (boxBottomRight?.value === CellValue.bomb) {
        numberAroundBomb++;
      }

      if (numberAroundBomb > 0) {
        caseBox[row][col] = { ...aroundBox, value: numberAroundBomb };
      }
    }
  }

  return caseBox;
};
