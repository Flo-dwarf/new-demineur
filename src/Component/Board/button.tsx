import { useState } from "react";
import { CellState, CellValue } from "../Enum/enum";
import "./board.css";

type DetailButton = {
  row: number;
  col: number;
  value: CellValue;
  state: CellState;
  click: void;
};

const Button = ({ row, col, value, state, click }: DetailButton) => {
  const [isActive, setActive] = useState(false);

  const handleActive = () => {
    if (state === CellState.hide) {
      setActive(!isActive);
    } else {
      setActive(isActive);
    }
  };

  // &#128681; FLAG

  return (
    <div
      onClick={handleActive}
      className={isActive ? "size revealed" : "size hide"}
    >
      {value === CellValue.bomb && isActive ? (
        <span role="img" className="icon" aria-label="bomb">
          &#128163;
        </span>
      ) : value !== CellValue.bomb &&
        isActive &&
        value !== CellValue.nothing ? (
        <span className={` icon value-${value}`}>{value}</span>
      ) : value === CellValue.nothing ? null : null}
    </div>
  );
};
export default Button;
