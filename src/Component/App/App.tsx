import React, { useEffect, useState } from "react";
import { generateBoard } from "../Board/board";
import Button from "../Board/button";
import { NUMBER_MINE } from "../Enum/values";
import Difficulty from "../Header/difficulty";
import Mine from "../Header/mine";
import Timer from "../Header/timer";
import "./App.css";

function App() {
  const [cell, setCell] = useState(generateBoard());
  const [startGame, setStartGame] = useState(false);
  const [counter, setCounter] = useState(0);
  
  // Timer fonctionnel
  useEffect(() => {
    if (startGame) {
      const timer = setInterval(() => setCounter(counter + 1), 1000);
      return () => clearInterval(timer);
    }
  }, [startGame, counter]);

  const resultBoard = () => {
    const result = cell.map((row, indexRow) =>
      row.map((box, indexCol) => (
        <Button
          key={`${indexRow}-${indexCol}`}
          row={indexRow}
          col={indexCol}
          value={box.value}
          state={box.state}
          click={handleStart()}
        />
      ))
    );
    return result;
  };

  const handleStart = () => {
    if (startGame === false) {
      setStartGame(!startGame);
    }
  };

  const handleReset = () => {
    setStartGame(false);
    setCounter(0);
    setCell(generateBoard());
    // setActive(false);
  };

  return (
    <div className="App">
      <div className="header">
        <Mine value={NUMBER_MINE} />
        <Difficulty click={handleReset} />
        <Timer value={counter} />
      </div>
      <div className="body">{resultBoard()}</div>
    </div>
  );
}

export default App;
