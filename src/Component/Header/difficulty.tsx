import "./.header.css";

const Difficulty = ({ click }: any) => {
  const value = <span>&#128512;</span>;

  return (
    <button className="buttonDifficulty" onClick={click}>
      {value}
    </button>
  );
};

export default Difficulty;
