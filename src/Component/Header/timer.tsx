import "./.header.css";

const Timer = ({ value }: any) => {
    return <div className="numberHeader">{value.toString().padStart(3, '0')}</div>;
  };
  
  export default Timer;
  