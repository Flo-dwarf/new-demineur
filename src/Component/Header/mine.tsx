import "./.header.css";

const Mine = ({ value }: any) => {
  return <div className="numberHeader">{value.toString().padStart(3, '0')}</div>;
};

export default Mine;
