export enum CellValue {
  nothing,
  one,
  two,
  three,
  four,
  five,
  six,
  seven,
  eight,
  bomb,
}

export enum CellState {
  open,
  hide,
  revealed,
  flag,
}

export type Cell = { value: CellValue; state: CellState };
